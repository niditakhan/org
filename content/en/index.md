---
title: Home 🏠
description: Grey Software is a not-for-profit organization on a mission to democratize software education.
category: Info
position: 1
---

In May 2020, a few volunteer alumni from UofT Mississauga 🇨🇦 were willing to help university students who wanted
authentic & up-to-date software education.

By September, this effort was formally incorporated as a not-for-profit organization in Canada under the name Grey
Software!

We envision a future where students and professionals collaborate on open-source software (OSS) that we can all trust,
love, and learn from!

We're getting there by making open products with students worldwide and sharing what we learn along the way!

## Our Plan

1. 🛠 Create a collection of OSS apps that people can trust, love, and learn from.

2. 📰 Publish original and curated educational content for free on our websites.

3. 🤝 Collaborate with small groups of students, volunteers, & contractors to extend our library of OSS products and
   educational content.

4. 💵 Generate revenue by offering exclusive features on our app collection & advertising space on our educational
   websites.

5. 🎓 Partner with universities and bootcamps to create a steady stream of students who collaborate on OSS to gain
   practical experience.

We're catalyzing our path to sustainability by offering lifetime recognition to organizations who would like to feature
their brand on our websites & repos.

<cta-button link="https://grey.software/pitch" text="Pitch"></cta-button>
<cta-button link="https://grey.software/pitch-detailed" text="Detailed Pitch"></cta-button>
<cta-button link="https://gitlab.com/groups/grey-software/-/roadmap" text="Full Roadmap"></cta-button>
