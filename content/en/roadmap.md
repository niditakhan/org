---
title: Roadmap
description: Grey Software's Roadmap
category: Info
position: 4
---

> The best place to see an interactive version of our roadmap is to visit our
> [Gitlab Roadmap Page](https://gitlab.com/groups/grey-software/-/roadmap)

<cta-button link="https://gitlab.com/groups/grey-software/-/roadmap" text="Full Roadmap"></cta-button>

## Overview

Our primary objectives are:

**🎯
[Gain credibility as a thought leader in software creation and education.](https://gitlab.com/groups/grey-software/-/epics/2)**

- 🥅 [Maintain software that people can trust, love, and learn from](https://gitlab.com/groups/grey-software/-/epics/18)

- 🥅
  [Build a useful, inspiring, and open software education ecosystem](https://gitlab.com/groups/grey-software/-/epics/6)
- 🥅 [Be a model organization for openness and transparency](https://gitlab.com/groups/grey-software/-/epics/1)

**🎯 [Generate sustainable revenue for our mission](https://gitlab.com/groups/grey-software/-/epics/16)**

- 🥅
  [Capital through Founding Partner Packages to Angel Investors and VCs](https://gitlab.com/groups/grey-software/-/epics/24)
- 🥅
  [Revenue by offering advertising space throughout our ecosystem](https://gitlab.com/groups/grey-software/-/epics/17)
- 🥅 [Revenue by offering App Collection Subscriptions](https://gitlab.com/groups/grey-software/-/epics/3)
- 🥅
  [Generate Revenue by Signing Education Program Contracts with Universities and Bootcamps](https://gitlab.com/groups/grey-software/-/epics/25)
